# fs-reader
Reads whole file, or just tail or head, only as needed (won't explode RAM)

```js
const reader = require('fs-reader');
//...
const file = '/test.txt';
const lines = 100;
reader(file, lines, function(err, contents){
    if(err) throw err;
    console.log(contents);
});
```

`file`: path to the file to read. Can be absolute or relative (from __dirname)

`lines`: Positive line number means head, negative means tail, 0 or null means whole.